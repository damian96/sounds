import sys
import struct as st
import binascii as ba
import bitarray as bt

code = {'0000' : '11110',
'0001': '01001',
'0010': '10100',
'0011': '10101',
'0100': '01010',
'0101': '01011',
'0110': '01110',
'0111': '01111',
'1000': '10010',
'1001': '10011',
'1010': '10110',
'1011': '10111',
'1100': '11010',
'1101': '11011',
'1110': '11100',
'1111': '11101'}

revcode = {'11110': '0000',
'01001': '0001',
'10100': '0010',
'10101': '0011',
'01010': '0100',
'01011': '0101',
'01110': '0110',
'01111': '0111',
'10010': '1000',
'10011': '1001',
'10110': '1010',
'10111': '1011',
'11010': '1100',
'11011': '1101',
'11100': '1110',
'11101': '1111'}

def encode(fr, to, msg):
    #przygotowanie skladnikow
    fri = int(fr)
    toi = int(to)
    arg1 = st.pack('>hi', 0, fri)
    arg2 = st.pack('>hi', 0, toi)
    arg3 = st.pack('>h', len(msg))
    # suma crc32
    tmp = ba.crc32(arg2)
    tmp = ba.crc32(arg1, tmp)
    tmp = ba.crc32(arg3, tmp)
    tmp = ba.crc32(msg, tmp)
    arg4 = st.pack('>i', tmp)
    #zamiana na bity
    a = bt.bitarray()
    a.frombytes(arg2)
    a.frombytes(arg1)
    a.frombytes(arg3)
    a.frombytes(msg)
    a.frombytes(arg4)
    leng = len(a)
    #zamiana na 4b5b
    i = 0
    cod = ''
    while i < leng/4:
        wycinek = a[i*4:(i+1)*4].to01()
        cod = cod + code[wycinek]
        i += 1
    leng = len(cod)
    #zamiana na nrz
    nrz = ''
    previous = cod[0]
    i = 1
    nrz = '0'
    while i<leng:
        if nrz[i-1]=='1':
            if cod[i]=='1':
                nrz += '0'
            else:
                nrz += '1'
        else:
            nrz += cod[i]
        i += 1
    wynik = ''
    wynik += '10101010'*7
    wynik += '10101011'
    wynik += nrz
    print wynik

def decode(a):
    if len(a)<26*8:
        return
    wynik = ''
    length = len(a)
    i = 65
    prev = ''
    prev += '1'
    while i<length:
        if a[i]==a[i-1]:
            prev += '0'
        else:
            prev += '1'
        i += 1
    length -= 64
    i = 0
    while i<length-5:
        t = revcode[str(prev[i:i+5])]
        wynik += t
        i += 5
    if((i+5)!=length):
        return
    else:
        t = revcode[str(prev[i:i+5])]
        wynik += t
    # potem pozamieniac 5 na 4
    # i dalej tak jak w C
    tmp = bt.bitarray(wynik).tobytes()
    le = len(tmp)
    if le<18: # 6+6+2+4...
        return
    x = tmp[2:6]
    arg1 = st.unpack('>i', x)
    y = tmp[8:12]
    arg2 = st.unpack('>i', y)
    z = tmp[12:14]
    arg3 = st.unpack('>h', z)
    if le!=18+arg3[0]:
        return
    msg = tmp[14:14+arg3[0]]
    u = tmp[14+arg3[0]:18+arg3[0]]
    arg4 = st.unpack('>i', u)
    crc2 = ba.crc32(x)
    crc2 = ba.crc32(y, crc2)
    crc2 = ba.crc32(z, crc2)
    crc2 = ba.crc32(msg, crc2)
    print arg2[0], arg1[0], msg

def ex():
    data = sys.stdin.read().splitlines()
    for line in data:
        x = []
        x = line.split(' ', 3)
        if x[0]=='E':
            encode(x[1], x[2], x[3])
        if x[0]=='D':
            decode(x[1])

ex()

'''
if(crc2==arg4[0]):
    print arg1[0], arg2[0], msg
else:
    return
'''
