#!/usr/bin/env python
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import struct as st
import binascii as ba
import bitarray as bt
import pulseaudio as pa
import numpy as np

revcode = {'11110': '0000',
'01001': '0001',
'10100': '0010',
'10101': '0011',
'01010': '0100',
'01011': '0101',
'01110': '0110',
'01111': '0111',
'11100': '1110',
'10010': '1000',
'10011': '1001',
'10110': '1010',
'10111': '1011',
'11010': '1100',
'11011': '1101',
'11101': '1111'}

def receive(time, freq1, freq2):
    with pa.simple.open(direction=pa.STREAM_RECORD, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as recorder:
        tmp = 44100/(int(time))
        nrz = ''
        data = []
        moc = []
        wynik = []
        wait = 0
        for i in xrange(5):
            wynik = []
            moc = []
            while len(wynik) < 10:
                data = recorder.read(tmp)
                w = np.fft.fft(data)
                w = w[range(int(len(w)/2))]
                val = np.argmax(np.abs(w))
                z = val*int(time)
                if(np.abs(w[val]) < 50000):
                    del wynik[:]
                    del moc[:]
                    continue
                if z == int(freq1):
                    wynik.append('0')
                    moc.append(np.abs(w[val]))
                elif z==int(freq2):
                    wynik.append('1')
                    moc.append(np.abs(w[val]))
                else:
                    del wynik[:]
                    del moc[:]
                    continue
                data = recorder.read(tmp/10)
                data = []
        max = 0
        nrMax = 0
        for i in xrange(10):
            if moc[i]>max:
                max = moc[i]
                nrMax = i
        wynik = []
        recorder.read((tmp/10)*(nrMax))
        i = 0
        while i < 2:
            data = recorder.read(tmp)
            w = np.fft.fft(data)
            w = w[range(int(len(w)/2))]
            val = np.argmax(np.abs(w))
            z = val*int(time)
            i += 1
            if z == int(freq1):
                wynik.append('0')
            elif z==int(freq2):
                wynik.append('1')

        while not ((wynik[i-2]=='1') and (wynik[i-1]=='1')):
            data = recorder.read(tmp)
            w = np.fft.fft(data)
            w = w[range(int(len(w)/2))]
            val = np.argmax(np.abs(w))
            z = val*int(time)
            i += 1
            if z == int(freq1):
                wynik.append('0')
            elif z==int(freq2):
                wynik.append('1')

        data = recorder.read(tmp)
        while len(data):
            w = np.fft.fft(data)
            w = w[range(int(len(w)/2))]
            val = np.argmax(np.abs(w))
            z = val*int(time)
            if np.abs(w[val]) < 150000:
                break
            if z == int(freq1):
                nrz += '0'
            elif z==int(freq2):
                nrz += '1'
            else:
                break
            data = recorder.read(tmp)
    retNrz = ''
    retNrz += 7*'10101010'
    retNrz += '10101011'
    retNrz += nrz
    return retNrz

def decode(nrz):
    if len(nrz)<18*8:
        return
    wynik = ''
    length = len(nrz)
    i = 65
    prev = ''
    prev += '1'
    while i<length:
        if nrz[i]==nrz[i-1]:
            prev += '0'
        else:
            prev += '1'
        i += 1
    length -= 64
    i = 0
    while i<length-5:
        t = revcode[str(prev[i:i+5])]
        wynik += t
        i += 5
    if((i+5)!=length):
        return
    else:
        t = revcode[str(prev[i:i+5])]
        wynik += t
    tmp = bt.bitarray(wynik).tobytes()
    le = len(tmp)
    if le<18:
        return
    x = tmp[0:6]
    arg1 = st.unpack('>hi', x)
    x2 = tmp[2:6]
    arg12 = st.unpack('>i', x2)
    y = tmp[6:12]
    arg2 = st.unpack('>hi', y)
    y2 = tmp[8:12]
    arg22 = st.unpack('>i', y2)
    z = tmp[12:14]
    arg3 = st.unpack('>h', z)
    if le!=18+arg3[0]:
        return
    msg = tmp[14:14+arg3[0]]
    u = tmp[14+arg3[0]:18+arg3[0]]
    arg4 = st.unpack('>i', u)
    crc2 = ba.crc32(x)
    crc2 = ba.crc32(y, crc2)
    crc2 = ba.crc32(z, crc2)
    crc2 = ba.crc32(msg, crc2)
    if crc2==arg4[0]:
        print arg22[0], arg12[0], msg

def ex():
    nrz = ''
    nrz = receive(sys.argv[1], sys.argv[2], sys.argv[3])
    decode(nrz)

ex()
