#!/usr/bin/env python
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import wave
import pulseaudio as pa
import math as np

def data(frequency, sample_rate=44100, amplitude=22050):
    frames = []
    for i in range(sample_rate/frequency):
        frames.append(np.sin(2*np.pi*i*frequency/sample_rate)*amplitude)
    return frames

with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
    i = 0
    x = (int(sys.argv[1]))*int(sys.argv[2]);
    while i<x:
        sample_data = data(int(sys.argv[1]))
        i = i + 1
        #print sample_data
        player.write(sample_data)
    player.drain()

'''
try:
    wav = wave.open(sys.argv[1], 'r')
    (nchannels, sampwidth, framerate, nframes, comptype, compname) = wav.getparams()
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        while True:
            frames = []
            for i in xrange(100):
                frames.append(22050*np.sin(2*np.pi*rate*i))
            player.write(frames)
        player.drain()
finally:
    wav.close()

sample_map = {
    1 : pa.SAMPLE_U8,
    2 : pa.SAMPLE_S16LE,
    4 : pa.SAMPLE_S32LE,
}
sample_type = {
    1 : np.uint8,
    2 : np.int16,
    4 : np.int32,
}
          frames = wav.readframes(100)
          frames = np.fromstring(frames, dtype=sample_type[sampwidth]).astype(np.float)
          if len(frames) == 0:
              break
'''


'''
import sys
import wave
import pulseaudio as pa
import numpy as np

def data(frequency, sample_rate=44100, amplitude=22050):
    frames = []
    for i in range(sample_rate/frequency):
        frames.append(np.sin(2*np.pi*i*frequency/sample_rate)*amplitude)
    return frames

with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
    i = 0
    x = (int(sys.argv[1]))*int(sys.argv[2]);
    dat = []
    length = 0
    while i<x:
        sample_data = data(int(sys.argv[1]))
        i = i + 1
        dat = dat + sample_data
        player.write(sample_data)
    player.drain()
    w = np.fft.fft(dat)
    w = w[range(int(len(w)/2))]
    idx = np.argmax(np.abs(w))
    print idx/int(sys.argv[2])
'''
