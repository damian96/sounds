#include<iostream>
#include<string>
using namespace std;
#include "pietras.h"
//#include "solution.h"

int main()
{
   int z, a, x, y;
   string s;
   
   cin >> z;
   while (z--)
   {
      queue Q;
      cin >> a;
      while (a--)
      {      
         cin >> s;
         switch(s[1])
         {
            case 'e': 
               if (s[2]=='m') { if (Q.empty()) cout << "EMPTY" << endl; else cout << "NO" << endl; }
               else 
               {
                  cin >> x >> y;
                  point p (x,y);
                  Q.enqueue( p );
               }
               break;
            case 'd': 
               {  
                  Q.dequeue().print();
                  break;
               }
            case 'f': 
               {  
                  if (s[2]=='u') { if (Q.full()) cout << "FULL" << endl; else cout << "NO" << endl; }
                  else  Q.front().print();   
                  break;                               
               }
            case 'c':                  
               Q.clear();
               break;
            case 'r':
               int r;  
               cin >> r;  
               Q.resize(r);
         };
      }  
   }
   return 0;
}
