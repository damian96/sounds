#!/usr/bin/env python
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import wave
import numpy as np
import pulseaudio as pa

with pa.simple.open(direction=pa.STREAM_RECORD, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as recorder:
    data = []
    nframes = int(sys.argv[1]) * 44100
    data = recorder.read(nframes)
    w = np.fft.fft(data)
    w = w[range(int(len(w)/2))]
    idx = np.argmax(np.abs(w))
    print idx/int(sys.argv[1])

'''
domyslnie bierze z mikrofonu, wiec jak jest bardzo cicho albo nie masz mikrofonu to masz same zera a jak zrobisz:
__PULSEAUDIO_WAVFILE__=440.wav ./recorder.py 5
to odczyta z pliku 440.wav
plik 440.wav mozesz sobie wygenerowac playerem
tak:
__PULSEAUDIO_WAVFILE__=440.wav ./player.py 5
i wtedy zapisze do tego pliku 440.wav
'''
