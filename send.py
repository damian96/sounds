#!/usr/bin/env python
# vim:ts=4:sts=4:sw=4:expandtab

import sys
import struct as st
import binascii as ba
import bitarray as bt
import pulseaudio as pa
import math as np

code = {'0000' : '11110',
'0001': '01001',
'0010': '10100',
'0011': '10101',
'0100': '01010',
'0101': '01011',
'0110': '01110',
'0111': '01111',
'1000': '10010',
'1001': '10011',
'1010': '10110',
'1011': '10111',
'1100': '11010',
'1101': '11011',
'1110': '11100',
'1111': '11101'}

def encode(fr, to, msg):
    fri = int(fr)
    toi = int(to)
    # poprzez zero uzupelniam nadawce i odbiorce zerami do 6 bajtow
    arg1 = st.pack('>hi', 0, fri)
    arg2 = st.pack('>hi', 0, toi)
    arg3 = st.pack('>h', len(msg))
    # wyliczanie sumy kontrolnej
    tmp = ba.crc32(arg2)
    tmp = b a.crc32(arg1, tmp)
    tmp = ba.crc32(arg3, tmp)
    tmp = ba.crc32(msg, tmp)
    arg4 = st.pack('>i', tmp)
    # kodowanie do ciagu bitow
    a = bt.bitarray()
    a.frombytes(arg2)
    a.frombytes(arg1)
    a.frombytes(arg3)
    a.frombytes(msg)
    a.frombytes(arg4)
    leng = len(a)
    i = 0
    cod = ''
    # kodowanie 4b5b
    while i < leng/4:
        wycinek = a[i*4:(i+1)*4].to01()
        cod = cod + code[wycinek]
        i += 1
    leng = len(cod)
    nrz = ''
    previous = cod[0]
    i = 1
    # kodowanie NRZ-I
    nrz = '0'
    while i<leng:
        if nrz[i-1]=='1':
            if cod[i]=='1':
                nrz += '0'
            else:
                nrz += '1'
        else:
            nrz += cod[i]
        i += 1
    wynik = ''
    wynik += '10101010'*7
    wynik += '10101011'
    wynik += nrz
    return wynik

def send(time, freq1, freq2, nrz):
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        le = len(nrz)
        for j in xrange(le):
            frames = []
            if nrz[j]=='1':
                for x in xrange(44100/int(time)):
                    frames.append(22050*np.sin(float(x)/44100*2*np.pi*int(freq2)))
                player.write(frames)
            else:
                for x in xrange(44100/int(time)):
                    frames.append(22050*np.sin(float(x)/44100*2*np.pi*int(freq1)))
                player.write(frames)
        player.drain()

def ex():
    data = sys.stdin.read().splitlines()
    for line in data:
        x = []
        nrz = ''
        x = line.split(' ', 2)
        nrz = encode(x[0], x[1], x[2])
        send(sys.argv[1], sys.argv[2], sys.argv[3], nrz)

ex()
