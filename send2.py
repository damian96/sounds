1) musisz wyprodukować dla każdego bitu framerate/time klatek (w naszym przykładzie 44100/10) dlaczego? 44100 to jedna sekunda…
więc jeden bit ma trwać 1/10 sekundy
2) jak generujesz:
amp*np.sin(x*2pi*freq), gdzie x to zmienna która iteruje się w przedziale od 0 do 1/time i ma się ziterować
44100/time razy (żeby mieć tyle klatek co trza)
czyli w sumie można i tak
for x in xrange(framerate/time):
… = amp*np.sin(x/framerate*2pi*freq)
i to tylko raz dla jednego całego bitu
3) nie zapomnij że freq1 to freq dla 0, freq2 to freq dla 1



def send(time, freq1, freq2, nrz):
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        le = len(nrz)
        for j in xrange(le):
            frame = []
            if nrz[j]=='1':
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*freq1/44100)*22050)
                    i = i + 1
                player.write(frames)
            else:
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*freq2/44100)*22050)
                    i = i + 1
                player.write(frames)
        player.drain()


def data(frequency, sample_rate=44100, amplitude=22050):
    frames = []
    for i in range(sample_rate/frequency):
        frames.append(np.sin(2*np.pi*i*frequency/sample_rate)*amplitude)
    return frames


def send(time, freq1, freq2, nrz):
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        le = len(nrz)
        for j in xrange(le):
            frames = []
            if nrz[j]=='1':
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*int(freq1)/44100)*22050)
                    i = i + 1
                player.write(frames)
            else:
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*int(freq2)/44100)*22050)
                    i = i + 1
                player.write(frames)
        player.drain()



def send(time, freq1, freq2, nrz):
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        le = len(nrz)
        for j in xrange(le):
            frames = []
            if nrz[j]=='1':
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*int(freq1)/44100)*22050)
                    i = i + 1
                player.write(frames)
            else:
                i = 0
                x = 44100/(int(time))
                while i<x:
                    frames.append(np.sin(2*np.pi*i*int(freq2)/44100)*22050)
                    i = i + 1
                player.write(frames)
        player.drain()



def send(time, freq1, freq2, nrz):
    with pa.simple.open(direction=pa.STREAM_PLAYBACK, format=pa.SAMPLE_S16LE, rate=44100, channels=1) as player:
        le = len(nrz)
        for j in xrange(le):
            if nrz[j]=='1':
                i = 0
                x = (int(freq2))/(int(time))
                while i<x:
                    sample_data = data(int(freq1))
                    i = i + 1
                    player.write(sample_data)
            else:
                i = 0
                x = (int(freq2))/(int(time))
                while i<x:
                    sample_data = data(int(freq2))
                    i = i + 1
                    player.write(sample_data)
        player.drain()
